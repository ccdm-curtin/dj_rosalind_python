"""
Functions for working with MSA like data (including profiles etc).

The msa "object" is a dict with the following elements:

    - ids: [id, id, ... ]
    - columns: [column, column] # list of lists


As in other modules, I have deliberately avoided classes to make communication
easier to team members.
"""


import copy

from .seq import seq_count


def seq_to_msa(seqs):
    """ Convert a list of sequences to an MSA object.

    Takes a list of dicts, each containing at least the key 'seq'.
    """

    output = {
        "ids": [seq["id"] if ("id" in seq) else None for seq in seqs],
        "columns": [],
        }

    # Rather than finding the seq length up front just add the columns as
    # we go.
    for seq in seqs:
        for i, base in enumerate(seq["seq"]):
            try:
                output["columns"][i].append(base)
            except IndexError:
                output["columns"].append([base])

    return output


def msa_profile(msa, counter=seq_count, alphabet=None):
    """ Convenience wrapper to map the count function """
    return [
        counter({"seq": column}, alphabet=alphabet)
        for column
        in msa["columns"]
        ]


def profile_consensus(prof):
    """ Find the consensus sequence given a sequence profile. """
    output = []
    for col in prof:
        max_ = max(col.values())
        for k, v in col.items():
            if v == max_:
                winner = k
                break

        output.append(k)

    return "".join(output)
