"""
A utility package containing information about biological alphabets
and conversion between them.
"""


dna = "ATGC"
dna_ambiguous = {
    'R': "AG",
    'Y': "CT",
    'S': "GC",
    'W': "AT",
    'K': "GT",
    'M': "AC",
    'B': "CGT",
    'D': "AGT",
    'H': "ACT",
    'V': "ACG",
    'N': "ACTG",
    }
dna_iupac = dna + ''.join(dna_ambiguous.keys())

dna_complement = {
    'A': 'T',
    'T': 'A',
    'G': 'C',
    'C': 'G',
    'R': 'Y',
    'Y': 'R',
    'S': 'W',
    'W': 'S',
    'K': 'M',
    'M': 'K',
    'B': 'V',
    'V': 'B',
    'D': 'H',
    'H': 'D',
    'N': 'N'
    }


def transcribe(sequence):
    """ Transcribes DNA into RNA. """
    return sequence.replace('T', 'U')

rna = transcribe(dna)
rna_iupac = transcribe(dna_iupac)
rna_ambiguous = {k: transcribe(v) for k, v in dna_ambiguous.items()}

amino_acids = 'ACDEFGHIKLMNOPQRSTUVWY*'

# Ambiguous codes from wikipedia
amino_acids_ambiguous = {
    'X': amino_acids,
    'B': "DN",
    'Z': "EQ",
    'J': "IL",
    'Φ': "VILFWYM", # Hydrophobic
    'Ω': "FWYH", # Aromatic
    'Ψ': "VILM", # Aliphatic
    'π': "PGAS", # Small
    'ζ': "STHNQEDKR", # Hydrophilic
    '+': "KRH", # Positive
    '-': "DE", # Negative
    '−': "DE", # (Unicode) negative
    }

amino_acids_iupac = amino_acids + ''.join(amino_acids_ambiguous.keys())

amino_acid_mass = {
    'A': 71.03711,
    'C': 103.00919,
    'D': 115.02694,
    'E': 129.04259,
    'F': 147.06841,
    'G': 57.02146,
    'H': 137.05891,
    'I': 113.08406,
    'K': 128.09496,
    'L': 113.08406,
    'M': 131.04049,
    'N': 114.04293,
    'P': 97.05276,
    'Q': 128.05858,
    'R': 156.10111,
    'S': 87.03203,
    'T': 101.04768,
    'V': 99.06841,
    'W': 186.07931,
    'Y': 163.06333,
    }

dna_codons = {
    "ATT": 'I',
    "ATC": 'I',
    "ATA": 'I',
    "CTT": 'L',
    "CTC": 'L',
    "CTA": 'L',
    "CTG": 'L',
    "TTA": 'L',
    "TTG": 'L',
    "GTT": 'V',
    "GTC": 'V',
    "GTA": 'V',
    "GTG": 'V',
    "TTT": 'F',
    "TTC": 'F',
    "ATG": 'M',
    "TGT": 'C',
    "TGC": 'C',
    "GCT": 'A',
    "GCC": 'A',
    "GCA": 'A',
    "GCG": 'A',
    "GGT": 'G',
    "GGC": 'G',
    "GGA": 'G',
    "GGG": 'G',
    "CCT": 'P',
    "CCC": 'P',
    "CCA": 'P',
    "CCG": 'P',
    "ACT": 'T',
    "ACC": 'T',
    "ACA": 'T',
    "ACG": 'T',
    "TCT": 'S',
    "TCC": 'S',
    "TCA": 'S',
    "TCG": 'S',
    "AGT": 'S',
    "AGC": 'S',
    "TAT": 'Y',
    "TAC": 'Y',
    "TGG": 'W',
    "CAA": 'Q',
    "CAG": 'Q',
    "AAT": 'N',
    "AAC": 'N',
    "CAT": 'H',
    "CAC": 'H',
    "GAA": 'E',
    "GAG": 'E',
    "GAT": 'D',
    "GAC": 'D',
    "AAA": 'K',
    "AAG": 'K',
    "CGT": 'R',
    "CGC": 'R',
    "CGA": 'R',
    "CGG": 'R',
    "AGA": 'R',
    "AGG": 'R',
    "TAA": '*',
    "TAG": '*',
    "TGA": '*',
}

rna_codons = {transcribe(k): v for k, v in dna_codons.items()}


def translate(sequence, table=rna_codons, include_stop=True):
    """ Translates a nucleotide sequence to an AA sequence.

    Keyword arguments:
    sequence -- A string of nucleotide bases.
    table -- A dict converting codons to AAs.
    include_stop -- True or false, if false when stop encountered return
        sequence immediately. If true continues reading and inserts '*' at
        stops.

    Returns:
    String of AAs
    """

    output = []
    for i in range(0, len(sequence), 3):
        codon = sequence[i:i+3]
        try:
            aa = table[codon]
        except KeyError as e:
            if len(codon) < 3:
                break
            else:
                raise e
        if aa == '*' and not include_stop:
            break
        output.append(aa)

    return "".join(output)
