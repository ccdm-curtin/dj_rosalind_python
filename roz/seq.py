"""
Functions for basic parsing and manipulating of sequence data.
Functions are all based on a dict object containing minimally, an `id` and a 
`seq` value.

Note I have deliberately avoided class-based implementations of these things
for ease of interpretation by beginners.
"""


import copy
from .alphabet import transcribe
from .alphabet import dna_complement
from .alphabet import translate

from .alphabet import rna_codons


def parse_fasta(handle, alphabet=None):
    """ Parses fasta formatted files.

    Takes a file like object and returns a list of dicts.
    """
    output = []

    # Initialise our storage
    record = {"id": None, "seq": ""}

    if alphabet is not None:
        record["alphabet"] = alphabet

    for line in handle:
        # Stip any extra whitespace on the line.
        sline = line.strip()

        if sline.startswith('>'):
            if record["id"] is not None:
                output.append(record)

            record = dict()
            if alphabet is not None:
                record["alphabet"] = alphabet

            record["id"] = sline.lstrip("> ")
            record["seq"] = ""
        else:
            record["seq"] += sline

    # Add the final sequence to the output
    output.append(record)

    return output


def seq_count(sequence, alphabet=None):
    """ Loops through a string (DNA) and returns a dictionary of base counts.

    Keyword arguments:
    sequence -- A dict containing the id and seq fields.
    alphabet -- (optional) a string denoting the alphabet to count.
      Used to ensure that certain letters are included (with 0 count) even if 
      not encountered in the sequence.
    """

    # Initialise my dictionary.
    if alphabet is None:
        counts = dict()
    else:
        counts = {base: 0 for base in alphabet}

    # Loop though the sequence and increment the count on each occurrence.
    for base in sequence["seq"]:
        try:
            counts[base] += 1
        except KeyError:
            counts[base] = 1

    return counts


def seq_transcribe(sequence, fn=transcribe):
    """ Transcribes DNA seq object into RNA. """

    new_seq = copy.deepcopy(sequence)
    new_seq["seq"] = transcribe(new_seq["seq"])
    new_seq["alphabet"] = "rna"
    return new_seq


def seq_translate(sequence, table, include_stop=True):
    """ Translate a sequence from nucleotides to AAs. """
    new_seq = copy.deepcopy(sequence)
    #print(new_seq)
    new_seq["seq"] = translate(new_seq["seq"], table, include_stop)
    new_seq["alphabet"] = "amino_acid"
    return new_seq


def reverse_complement(sequence, table=dna_complement):
    """ Finds reverse complement of DNA sequence.

    Keyword arguments:
    sequence -- A dict with the keys id and seq
    table -- A dict mapping bases to their complementary bases.
    """

    new_seq = copy.deepcopy(sequence)

    # Use a list comprehension to convert each base to its complementary base.
    comp_seq = [table[base] for base in sequence["seq"]]

    # Reverse the sequence using the slice syntax.
    # You could also use `comp_seq.reverse()` to edit in-place.
    rev_comp_seq = comp_seq[::-1]

    # Join the characters in the list to get a string back.
    new_seq["seq"] = ''.join(rev_comp_seq)
    return new_seq


def seq_find_all(super_seq, sub_seq):
    """ Finds the indices of each occurrence of a substring. """

    matches = list()

    super_string = super_seq["seq"]
    sub_string = sub_seq["seq"]

    for i in range(0, len(super_string) - len(sub_string) + 1):
        match = True
        for j in range(0, len(sub_string)):
            if super_string[i + j] != sub_string[j]:
                match = False
                break

        if match:
            matches.append(i + 1)

    return matches
