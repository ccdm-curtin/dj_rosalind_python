#!/usr/bin/env python3

import sys
import re
import copy

from roz.seq import parse_fasta


usage = """\
To test run:

$ pytest ./overlap_graph.py

To run the program on a new dataset run:

$ ./overlap_graph.py my_file.txt
"""


def prefixes(seqs, k=3):
    """ Finds each seqid and adds to dict keyed by prefix """
    output = dict()

    for seq in seqs:
        try:
            output[seq["seq"][:k]].append(seq["id"])
        except:
            output[seq["seq"][:k]] = [seq["id"]]

    return output


def suffixes(seqs, k=3):
    """ Finds each seqid and adds to dict keyed by suffix """
    output = dict()

    for seq in seqs:
        try:
            output[seq["seq"][(0 - k):]].append(seq["id"])
        except:
            output[seq["seq"][(0 - k):]] = [seq["id"]]

    return output


def test_overlap_graph():
    seqs = [
        {'id': 'Rosalind_0498', 'seq': 'AAATAAA'},
        {'id': 'Rosalind_2391', 'seq': 'AAATTTT'},
        {'id': 'Rosalind_2323', 'seq': 'TTTTCCC'},
        {'id': 'Rosalind_0442', 'seq': 'AAATCCC'},
        {'id': 'Rosalind_5013', 'seq': 'GGGTGGG'}
        ]

    expected = [
        ('Rosalind_0498', 'Rosalind_2391'),
        ('Rosalind_0498', 'Rosalind_0442'),
        ('Rosalind_2391', 'Rosalind_2323')
        ]

    sufs = suffixes(seqs, k=3)
    prefs = prefixes(seqs, k=3)
    actual = set(overlap_graph(sufs, prefs))

    assert len(actual) == len(expected)
    for pair in expected:
        assert pair in actual
    return


def overlap_graph(sufs, prefs):
    pairs = []
    for prefix, prefix_seq_ids in prefs.items():
        try:
            for suffix_seq_id in sufs[prefix]:
                for prefix_seq_id in prefix_seq_ids:
                    if prefix_seq_id != suffix_seq_id:
                        pairs.append( (suffix_seq_id, prefix_seq_id) )

        except KeyError:
            continue
    return pairs

def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    file_path = sys.argv[1]
    with open(file_path) as handle:
        seqs = parse_fasta(handle)

    sufs = suffixes(seqs, k=3)
    prefs = prefixes(seqs, k=3)

    pairs = overlap_graph(sufs, prefs)
    for pair in pairs:
        print(*pair)

    return


if __name__ == "__main__":
    main()
