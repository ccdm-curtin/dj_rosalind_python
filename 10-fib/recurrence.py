#!/usr/bin/env python3

import sys

from roz.seq import parse_fasta
from roz.alphabet import translate
from roz.alphabet import rna_codons


usage = """\
To test run:

$ pytest ./distance.py

To run the program on a new dataset run:

$ ./distance.py my_file.txt
"""


def test_fib():
    n = 5
    k = 3

    expected = 19
    actual = fib(n, k)

    assert actual == expected
    return


def fib(n, k):
    if n in (1, 2):
        return 1

    previous_2 = 1
    previous_1 = 1

    for i in range(2, n):
        current = previous_1 + k * previous_2
        previous_2 = previous_1
        previous_1 = current
    return current


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 3:
        print(usage)
        sys.exit(1)

    n = int(sys.argv[1])
    k = int(sys.argv[2])

    print(fib(n, k))
    return


if __name__ == "__main__":
    main()
