#!/usr/bin/env python3

import sys

usage = """\
To test run:

$ pytest ./counting_nucleotides.py

To run the program on a new dataset run:

$ ./counting_nucleotides.py ATGCTAGAGGAG
"""

def test_count():
    example = (
        "AGCTTTTCATTCTGACTGCAACGGGCAATATG"
        "TCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"
        )
    expected = {
        'A': 20,
        'C': 12,
        'G': 17,
        'T': 21,
        }

    result = count(example)
    for base, c in expected.items():
        assert c == result[base]

    return


def count(sequence):
    """ Loops through a string (DNA) and returns a dictionary of base counts.

    NB: the question only calls for DNA input, so my counter is hard coded for
    clarity.
    """

    # Initialise my dictionary with values for each base.
    counts = {'A': 0, 'C': 0, 'G': 0, 'T': 0}

    # Loop though the sequence and increment the count on each occurrence.
    for base in sequence:
        counts[base] += 1

    return counts


def main():
    """ Takes sequence from the command line and prints results. """

    # We imported the sys package at the top of the package.
    # sys.argv contains all command line inputs
    # Here we print a usage message if the input isn't given.
    # NB the first element of argv is the executable name,
    # i.e. counting_nucleotides.py so add 1 to the count.
    if len(sys.argv) < 2:
        # If too few arguments
        # Print the usage string (top of script).
        print(usage)
        # Exit the program.
        sys.exit(1)

    # Get the first input from the command line (a string, our sequence).
    seq = sys.argv[1]

    # Count occurrence of each base.
    counts = count(seq)

    # Print the base frequencies in the correct order.
    # NB, when given multiple options, print will automatically add a space
    # between them.
    print(counts['A'], counts['C'], counts['G'], counts['T'])
    return


# If we are calling the program from the command line (i.e. it's
# not being imported from another python script, then we run this.
if __name__ == "__main__":
    main()
