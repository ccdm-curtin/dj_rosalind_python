#!/usr/bin/env python3

import sys

usage = """\
To test run:

$ pytest ./gc_content.py

To run the program on a new dataset run:

$ ./motif_finding.py GATATATGCATATACTT ATAT
"""


def test_find_substring():
    super_string = "GATATATGCATATACTT"
    sub_string = "ATAT"

    # Should really include a case for matches at end of superstring
    expected = [2, 4, 10]

    actual = find_substring(super_string, sub_string)

    assert len(expected) == len(actual)

    for i, idx in enumerate(expected):
        assert idx == actual[i]

    return

def find_substring(super_string, sub_string):
    """ Finds the indices of each occurrence of a substring. """

    matches = list()

    for i in range(0, len(super_string) - len(sub_string) + 1):
        match = True
        for j in range(0, len(sub_string)):
            if super_string[i + j] != sub_string[j]:
                match = False
                break

        if match:
            matches.append(i + 1)

    return matches


def main():
    """ Takes sequence from the command line and prints results. """

    # Check the command line inputs.
    # This time we're taking 2 args
    if len(sys.argv) < 3:
        print(usage)
        sys.exit(1)

    super_string = sys.argv[1]
    sub_string = sys.argv[2]

    # One condition in the question was that the superstring must
    # always be longer than the substring. We can make sure like this...
    assert len(sub_string) < len(super_string), \
        "Substring can't be smaller than super string"

    # Find the locations of each substring match
    matches = find_substring(super_string, sub_string)

    # Print the output. Space delimited
    print(" ".join([str(m) for m in matches]))
    return

if __name__ == "__main__":
    main()
