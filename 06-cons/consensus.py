#!/usr/bin/env python3

import sys

usage = """\
To test run:

$ pytest ./consensus.py

To run the program on a new dataset run:

$ ./consensus.py my.fasta
"""


def test_fasta_parser():
    example = """\
>Rosalind_6404
CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC
TCCCACTAATAATTCTGAGG
>Rosalind_5959
CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT
ATATCCATTTGTCAGCAGACACGC
>Rosalind_0808
CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC
TGGGAACCTGCGGGCAGTAGGTGGAAT
"""

    expected = {
        "Rosalind_6404": ("CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTT"
                          "CCGGCCTTCCCTCCCACTAATAATTCTGAGG"),
        "Rosalind_5959": ("CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCC"
                          "GCCGAAGGTCTATATCCATTTGTCAGCAGACACGC"),
        "Rosalind_0808": ("CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGA"
                          "CCAGCCCGGACTGGGAACCTGCGGGCAGTAGGTGGAAT")
        }

    seqs = fasta_parser(example.split('\n'))
    for seq_id, seq in expected.items():
        assert seqs[seq_id] == seq
    return


def fasta_parser(handle):
    output = dict()

    # Initialise our storage
    seq_id = None
    seq = ""

    for line in handle:
        # Stip any extra whitespace on the line.
        sline = line.strip()

        if sline.startswith('>'):
            if seq_id is not None:
                output[seq_id] = seq

            seq_id = sline.lstrip("> ")
            seq = ""
        else:
            seq += sline

    # Add the final sequence to the output
    output[seq_id] = seq

    return output


def test_columns():
    seqs = {
        "Rosalind_1": "ATCCAGCT",
        "Rosalind_2": "GGGCAACT",
        "Rosalind_3": "ATGGATCT",
        "Rosalind_4": "AAGCAACC",
        "Rosalind_5": "TTGGAACT",
        "Rosalind_6": "ATGCCATT",
        "Rosalind_7": "ATGGCACT"
        }

    expected = [
        ['A', 'G', 'A', 'A', 'T', 'A', 'A'],
        ['T', 'G', 'T', 'A', 'T', 'T', 'T'],
        ['C', 'G', 'G', 'G', 'G', 'G', 'G'],
        ['C', 'C', 'G', 'C', 'G', 'C', 'G'],
        ['A', 'A', 'A', 'A', 'A', 'C', 'C'],
        ['G', 'A', 'T', 'A', 'A', 'A', 'A'],
        ['C', 'C', 'C', 'C', 'C', 'T', 'C'],
        ['T', 'T', 'T', 'C', 'T', 'T', 'T'],
        ]

    actual = columns(seqs)

    for i, col in enumerate(expected):
        assert col == actual[i]
    return


def columns(seqs):
    """ Converts a dict of sequences to column based list of seqs.
    """

    output = list()

    first = True
    for seq in seqs.values():
        for i, base in enumerate(seq):
            if first:
                output.append([base])
            else:
                output[i].append(base)

        first = False

    return output


def test_count():
    example = (
        "AGCTTTTCATTCTGACTGCAACGGGCAATATG"
        "TCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"
        )
    expected = {
        'A': 20,
        'C': 12,
        'G': 17,
        'T': 21,
        }

    result = count(example)
    for base, c in expected.items():
        assert c == result[base]

    return


def count(sequence):
    """ Loops through a string (DNA) and returns a dictionary of base counts.
    """
    counts = {'A': 0, 'C': 0, 'G': 0, 'T': 0}

    for base in sequence:
        counts[base] += 1

    return counts


def test_profile():
    cols = [
        ['A', 'G', 'A', 'A', 'T', 'A', 'A'],
        ['T', 'G', 'T', 'A', 'T', 'T', 'T'],
        ['C', 'G', 'G', 'G', 'G', 'G', 'G'],
        ['C', 'C', 'G', 'C', 'G', 'C', 'G'],
        ['A', 'A', 'A', 'A', 'A', 'C', 'C'],
        ['G', 'A', 'T', 'A', 'A', 'A', 'A'],
        ['C', 'C', 'C', 'C', 'C', 'T', 'C'],
        ['T', 'T', 'T', 'C', 'T', 'T', 'T'],
        ]

    expected = [
        {'A': 5, 'C': 0, 'G': 1, 'T': 1},
        {'A': 1, 'C': 0, 'G': 1, 'T': 5},
        {'A': 0, 'C': 1, 'G': 6, 'T': 0},
        {'A': 0, 'C': 4, 'G': 3, 'T': 0},
        {'A': 5, 'C': 2, 'G': 0, 'T': 0},
        {'A': 5, 'C': 0, 'G': 1, 'T': 1},
        {'A': 0, 'C': 6, 'G': 0, 'T': 1},
        {'A': 0, 'C': 1, 'G': 0, 'T': 6},
        ]


    actual = profile(cols)

    for i, column in enumerate(expected):
        assert column == actual[i]
    return


def profile(cols, counter=count):
    """ Convenience wrapper to map the count function """
    return [count(column) for column in cols]


def test_consensus():
    prof = [
        {'A': 5, 'C': 0, 'G': 1, 'T': 1},
        {'A': 1, 'C': 0, 'G': 1, 'T': 5},
        {'A': 0, 'C': 1, 'G': 6, 'T': 0},
        {'A': 0, 'C': 4, 'G': 3, 'T': 0},
        {'A': 5, 'C': 2, 'G': 0, 'T': 0},
        {'A': 5, 'C': 0, 'G': 1, 'T': 1},
        {'A': 0, 'C': 6, 'G': 0, 'T': 1},
        {'A': 0, 'C': 1, 'G': 0, 'T': 6},
        ]

    expected = "ATGCAACT"
    actual = consensus(prof)
    assert actual == expected
    return


def consensus(prof):
    """ Find the consensus sequence given a sequence profile. """
    output = []
    for col in prof:
        max_ = max(col.values())
        for k, v in col.items():
            if v == max_:
                winner = k
                break

        output.append(k)

    return "".join(output)


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    # Read the fasta file
    fasta_file = sys.argv[1]
    with open(fasta_file, "r") as handle:
        seqs = fasta_parser(handle)

    # Convert the multiple fastas into an MSA-like structure
    msa = columns(seqs)

    # Construct the sequence profile from the MSA
    prof = profile(msa)

    # Print the consensus sequence
    print(consensus(prof))

    # Print the profile in the required format
    for base in "ACGT":
        # Get the row value for each column
        row = [str(col[base]) for col in prof]
        # Print out the row, separated by spaces.
        print("{b}: {v}".format(b=base, v=' '.join(row)))
    return


if __name__ == "__main__":
    main()
