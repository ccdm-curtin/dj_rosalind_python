#!/usr/bin/env python3

import sys
import re
import copy
import pprint

from itertools import permutations

usage = """\
To test run:

$ pytest ./gene_orders.py

To run the program on a new dataset run:

$ ./gene_orders.py 3
"""


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    file_path = sys.argv[1]

    with open(file_path) as handle:
        n = int(next(handle))
        n_i = 0
        for line in handle:
            n_i += 1
    print(n - n_i - 1)
    return


if __name__ == "__main__":
    main()
