#!/usr/bin/env python3

import sys

usage = """\
To test run:

$ pytest ./reverse_complement.py

To run the program on a new dataset run:

$ ./reverse_complement.py ATGCTAGAGGAG
"""

def test_reverse_complement():
    example = "AAAACCCGGT"
    expected = "ACCGGGTTTT"

    result = reverse_complement(example)
    assert result == expected

    return


def reverse_complement(sequence):
    """ Finds reverse complement of DNA sequence.
    """

    # Simple map defining the complementary bases
    complement = {
        'A': 'T',
        'T': 'A',
        'G': 'C',
        'C': 'G'
        }

    # Use a list comprehension to convert each base to its complementary base.
    comp_seq = [complement[base] for base in sequence]

    # Reverse the sequence using the slice syntax.
    # You could also use `reverse(comp_seq)`.
    rev_comp_seq = comp_seq[::-1]

    # Join the characters in the list to get a string back.
    return ''.join(rev_comp_seq)


def main():
    """ Takes sequence from the command line and prints results. """

    # Check if input is given. Otherwise print usage.
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    seq = sys.argv[1]
    rc = reverse_complement(seq)
    print(rc)
    return


if __name__ == "__main__":
    main()
