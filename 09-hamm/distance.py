#!/usr/bin/env python3

import sys

from roz.seq import parse_fasta
from roz.alphabet import translate
from roz.alphabet import rna_codons


usage = """\
To test run:

$ pytest ./distance.py

To run the program on a new dataset run:

$ ./distance.py my_file.txt
"""


def test_hamming():
    s1 = "GAGCCTACTAACGGGAT"
    s2 = "CATCGTAATGACGGCCT"

    expected = 7
    actual = hamming(s1, s2)

    assert actual == expected
    return


def hamming(seq1, seq2):
    dist = 0
    for base1, base2 in zip(seq1, seq2):
        if base1 != base2:
            dist += 1
    return dist


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    file_path = sys.argv[1]
    with open(file_path) as handle:
        lines = handle.readlines()

    dist = hamming(lines[0], lines[1])
    print(dist)
    return


if __name__ == "__main__":
    main()
