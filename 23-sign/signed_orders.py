#!/usr/bin/env python3

import sys
import re
import copy

from itertools import permutations

usage = """\
To test run:

$ pytest ./gene_orders.py

To run the program on a new dataset run:

$ ./gene_orders.py 3
"""



def test_permute():
    examples = [
        [
            2,
            [
                [-1, -2],
                [-1, 2],
                [1, -2],
                [1, 2],
                [-2, -1],
                [-2, 1],
                [2, -1],
                [2, 1],
            ]
        ],
        [
            3,
            [
                [1, 2, 3],
                [1, -2, 3],
                [1, 2, -3],
                [1, -2, -3],
                [-1, 2, 3],
                [-1, -2, 3],
                [-1, 2, -3],
                [-1, -2, -3],
                [1, 3, 2],
                [1, -3, 2],
                [1, 3, -2],
                [1, -3, -2],
                [-1, 3, 2],
                [-1, -3, 2],
                [-1, 3, -2],
                [-1, -3, -2],
                [2, 1, 3],
                [2, -1, 3],
                [2, 1, -3],
                [2, -1, -3],
                [-2, 1, 3],
                [-2, -1, 3],
                [-2, 1, -3],
                [-2, -1, -3],
                [2, 3, 1],
                [2, -3, 1],
                [2, 3, -1],
                [2, -3, -1],
                [-2, 3, 1],
                [-2, -3, 1],
                [-2, 3, -1],
                [-2, -3, -1],
                [3, 1, 2],
                [3, -1, 2],
                [3, 1, -2],
                [3, -1, -2],
                [-3, 1, 2],
                [-3, -1, 2],
                [-3, 1, -2],
                [-3, -1, -2],
                [3, 2, 1],
                [3, -2, 1],
                [3, 2, -1],
                [3, -2, -1],
                [-3, 2, 1],
                [-3, -2, 1],
                [-3, 2, -1],
                [-3, -2, -1],
            ]
        ],
        ]

    for example in examples:
        n, expected = example
        actual = permute(n)
        assert len(expected) == len(actual)

        for perm in expected:
            assert perm in actual

    return

def generate(n, A):
    """ Using heaps algorithm https://en.wikipedia.org/wiki/Heap%27s_algorithm"""
    """
    procedure generate(n : integer, A : array of any):
        if n = 1 then
              output(A)
        else
            for i := 0; i < n - 1; i += 1 do
                generate(n - 1, A)
                if n is even then
                    swap(A[i], A[n-1])
                else
                    swap(A[0], A[n-1])
                end if
            end for
            generate(n - 1, A)
        end if
    """
    output = []
    if n == 1:
        output.append(copy.deepcopy(A))
    else:
        for i in range(n - 1):
            output.extend(generate(n - 1, copy.deepcopy(A)))

            if n % 2:
                tmp = A[i]
                A[i] = A[n - 1]
                A[n - 1] = tmp
            else:
                tmp = A[0]
                A[0] = A[n - 1]
                A[n - 1] = tmp
        output.extend(generate(n - 1, copy.deepcopy(A)))

    return output


def plus_minus(A):
    output = []
    for i in [1, -1]:
        this = copy.deepcopy([A[0] * i])
        if len(A) == 1:
            output.append(this)
            continue

        for suffix in plus_minus(copy.deepcopy(A[1:])):
            that = copy.deepcopy(this)
            that.extend(suffix)
            output.append(that)

    return output

def permute(n):

    block = list(range(1, n + 1))
    signed = plus_minus(block)
    output = []

    for s in signed:
        output.extend(generate(n, s))

    return output


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    n = int(sys.argv[1])


    results = permute(n)
    print(len(results))
    for result in results:
        print(" ".join([str(i) for i in result]))

    return


if __name__ == "__main__":
    main()
