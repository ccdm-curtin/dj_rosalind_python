"""
Tests for msa module
"""


from roz import msa
from roz import alphabet


def test_seq_to_msa():
    seqs = [
        {"id": "Rosalind_1", "seq": "ATCCAGCT"},
        {"id": "Rosalind_2", "seq": "GGGCAACT"},
        {"id": "Rosalind_3", "seq": "ATGGATCT"},
        {"id": "Rosalind_4", "seq": "AAGCAACC"},
        {"id": "Rosalind_5", "seq": "TTGGAACT"},
        {"id": "Rosalind_6", "seq": "ATGCCATT"},
        {"id": "Rosalind_7", "seq": "ATGGCACT"}
        ]

    expected = [
        ['A', 'G', 'A', 'A', 'T', 'A', 'A'],
        ['T', 'G', 'T', 'A', 'T', 'T', 'T'],
        ['C', 'G', 'G', 'G', 'G', 'G', 'G'],
        ['C', 'C', 'G', 'C', 'G', 'C', 'G'],
        ['A', 'A', 'A', 'A', 'A', 'C', 'C'],
        ['G', 'A', 'T', 'A', 'A', 'A', 'A'],
        ['C', 'C', 'C', 'C', 'C', 'T', 'C'],
        ['T', 'T', 'T', 'C', 'T', 'T', 'T'],
        ]

    actual = msa.seq_to_msa(seqs)

    for i, col in enumerate(expected):
        assert col == actual["columns"][i]

    for i, seq in enumerate(seqs):
        assert seq["id"] == actual["ids"][i]
    return


def test_msa_profile():
    cols = [
        ['A', 'G', 'A', 'A', 'T', 'A', 'A'],
        ['T', 'G', 'T', 'A', 'T', 'T', 'T'],
        ['C', 'G', 'G', 'G', 'G', 'G', 'G'],
        ['C', 'C', 'G', 'C', 'G', 'C', 'G'],
        ['A', 'A', 'A', 'A', 'A', 'C', 'C'],
        ['G', 'A', 'T', 'A', 'A', 'A', 'A'],
        ['C', 'C', 'C', 'C', 'C', 'T', 'C'],
        ['T', 'T', 'T', 'C', 'T', 'T', 'T'],
        ]

    expected = [
        {'A': 5, 'C': 0, 'G': 1, 'T': 1},
        {'A': 1, 'C': 0, 'G': 1, 'T': 5},
        {'A': 0, 'C': 1, 'G': 6, 'T': 0},
        {'A': 0, 'C': 4, 'G': 3, 'T': 0},
        {'A': 5, 'C': 2, 'G': 0, 'T': 0},
        {'A': 5, 'C': 0, 'G': 1, 'T': 1},
        {'A': 0, 'C': 6, 'G': 0, 'T': 1},
        {'A': 0, 'C': 1, 'G': 0, 'T': 6},
        ]


    actual = msa.msa_profile({"columns": cols}, alphabet=alphabet.dna)

    for i, column in enumerate(expected):
        assert column == actual[i]
    return


def test_profile_consensus():
    prof = [
        {'A': 5, 'C': 0, 'G': 1, 'T': 1},
        {'A': 1, 'C': 0, 'G': 1, 'T': 5},
        {'A': 0, 'C': 1, 'G': 6, 'T': 0},
        {'A': 0, 'C': 4, 'G': 3, 'T': 0},
        {'A': 5, 'C': 2, 'G': 0, 'T': 0},
        {'A': 5, 'C': 0, 'G': 1, 'T': 1},
        {'A': 0, 'C': 6, 'G': 0, 'T': 1},
        {'A': 0, 'C': 1, 'G': 0, 'T': 6},
        ]

    expected = "ATGCAACT"
    actual = msa.profile_consensus(prof)
    assert actual == expected
    return
