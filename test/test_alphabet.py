"""
Testing the alphabet module.
"""

from roz import alphabet as alph


def test_transcribe():
    example = "GATGGAACTTGACTACGTAAATT"
    expected = "GAUGGAACUUGACUACGUAAAUU"

    result = alph.transcribe(example)
    assert result == expected

    return


def test_translate():
    seq = "AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA"

    expected = "MAMAPRTEINSTRING"
    actual = alph.translate(seq, table=alph.rna_codons, include_stop=False)

    assert actual == expected
    return
