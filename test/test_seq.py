"""
Testing the seq module
"""

from roz import seq as mseq
from roz import alphabet

def test_parse_fasta():
    example = """\
>Rosalind_6404
CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC
TCCCACTAATAATTCTGAGG
>Rosalind_5959
CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT
ATATCCATTTGTCAGCAGACACGC
>Rosalind_0808
CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC
TGGGAACCTGCGGGCAGTAGGTGGAAT
"""

    expected = [
        {
            "id": "Rosalind_6404",
            "seq": ("CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTT"
                    "CCGGCCTTCCCTCCCACTAATAATTCTGAGG"),
        },
        {
            "id": "Rosalind_5959",
            "seq": ("CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCC"
                    "GCCGAAGGTCTATATCCATTTGTCAGCAGACACGC"),
        },
        {
            "id": "Rosalind_0808",
            "seq": ("CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGA"
                    "CCAGCCCGGACTGGGAACCTGCGGGCAGTAGGTGGAAT"),
        }
        ]

    seqs = mseq.parse_fasta(example.split('\n'))
    for i, _ in enumerate(expected):
        assert expected[i]["id"] == seqs[i]["id"]
        assert expected[i]["seq"] == seqs[i]["seq"]

    return


def test_seq_count():
    example = (
        "AGCTTTTCATTCTGACTGCAACGGGCAATATG"
        "TCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"
        )
    expected = {
        'A': 20,
        'C': 12,
        'G': 17,
        'T': 21,
        }

    result = mseq.seq_count({"seq": example})
    for base, c in expected.items():
        assert c == result[base]

    return


def test_seq_transcribe():
    example = "GATGGAACTTGACTACGTAAATT"
    expected = "GAUGGAACUUGACUACGUAAAUU"

    result = mseq.seq_transcribe({"seq": example})
    assert result["seq"] == expected
    return


def test_seq_translate():
    seq = "AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA"

    expected = "MAMAPRTEINSTRING"
    actual = mseq.seq_translate(
        {"seq": seq},
        table=alphabet.rna_codons,
        include_stop=False
        )

    assert actual["seq"] == expected
    return


def test_reverse_complement():
    example = "AAAACCCGGT"
    expected = "ACCGGGTTTT"

    result = mseq.reverse_complement({"seq": example})
    assert result["seq"] == expected
    return


def test_seq_find_all():
    super_string = "GATATATGCATATACTT"
    sub_string = "ATAT"

    # Should really include a case for matches at end of superstring
    expected = [2, 4, 10]

    actual = mseq.seq_find_all(
        {"seq": super_string},
        {"seq": sub_string}
        )

    assert len(expected) == len(actual)

    for i, idx in enumerate(expected):
        assert idx == actual[i]

    return
