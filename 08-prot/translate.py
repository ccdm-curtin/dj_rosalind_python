#!/usr/bin/env python3

import sys

from roz.seq import parse_fasta
from roz.alphabet import translate
from roz.alphabet import rna_codons


usage = """\
To test run:

$ pytest ./translate.py

To run the program on a new dataset run:

$ ./translate.py AUGAAUGCGCA
"""


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    seq = sys.argv[1]
    aas = translate(seq, rna_codons, False)
    print(aas)
    return


if __name__ == "__main__":
    main()
