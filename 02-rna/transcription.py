#!/usr/bin/env python3

import sys

usage = """\
To test run:

$ pytest ./transcription.py

To run the program on a new dataset run:

$ ./transcription.py ATGCTAGAGGAG
"""

def test_transcribe():
    example = "GATGGAACTTGACTACGTAAATT"
    expected = "GAUGGAACUUGACUACGUAAAUU"

    result = transcribe(example)
    assert result == expected

    return


def transcribe(sequence):
    """ Transcribes DNA into RNA.
    """

    trans = sequence.replace('T', 'U')
    return trans


def main():
    """ Takes sequence from the command line and prints results. """

    # Check that we've been given input.
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    seq = sys.argv[1]
    trans = transcribe(seq)
    print(trans)
    return


if __name__ == "__main__":
    main()
