#!/usr/bin/env python3

import sys
import re
import copy

from roz.alphabet import dna_codons


usage = """\
To test run:

$ pytest ./back_translation.py

To run the program on a new dataset run:

$ ./back_translation.py MCGGVA
"""


from roz.alphabet import translate
from roz.alphabet import dna_codons


codon_count = {}
for _, v in dna_codons.items():
    if v in codon_count:
        codon_count[v] += 1
    else:
        codon_count[v] = 1


def fold(fn, vals, init=1):

    for v in vals:
        init = fn(init, v)

    return init


def test_combinations():
    examples = [
        ("MA*",12),
        ]


    for seq, n in examples:
        actual = combinations(seq)

        assert actual == n

    return


def combinations(seq, counts=codon_count):

    seq_counts = (codon_count[aa] for aa in seq)
    product = fold(lambda x, y: (x * y) % 1_000_000, seq_counts)
    return product


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    seq = sys.argv[1]
    if seq == "-":
        seq = sys.stdin.read().replace("\n", "")

    c = combinations(seq + "*")
    print(c)

    return


if __name__ == "__main__":
    main()
