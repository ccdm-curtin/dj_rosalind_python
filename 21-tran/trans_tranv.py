#!/usr/bin/env python3

import sys
import copy

from roz.seq import parse_fasta
from roz.alphabet import translate
from roz.alphabet import rna_codons


usage = """\
To test run:

$ pytest ./trans_tranv.py

To run the program on a new dataset run:

$ ./trans_tranv.py my_file.fasta
"""

def test_trans():
    example = [
        {
        "id": "Rosalind_0209",
        "seq": "GCAACGCACAACGAAAACCCTTAGGGACTGGATTATTTCGTGATCGTTGTAGTTATTGGAAGTACGGGCATCAACCCAGTT"
        },
        {
        "id": "Rosalind_2200",
        "seq": "TTATCTGACAAAGAAAGCCGTCAACGGCTGGATAATTTCGCGATCGTGCTGGTTACTGGCGGTACGAGTGTTCCTTTGGGT"
        },
    ]

    expected = 1.21428571429

    actual = trans(example[0], example[1])

    assert (expected - actual) < 0.000001 and (actual - expected) < 0.000001
    return


def trans(seq1, seq2):

    transitions = {
        ('A', 'G'),
        ('G', 'A'),
        ('C', 'T'),
        ('T', 'C'),
        }

    nts = 0
    ntv = 0

    for pair in zip(seq1["seq"], seq2["seq"]):
        if pair[0] == pair[1]:
            continue
        elif pair in transitions:
            nts += 1
        else:
            ntv += 1

    if ntv == 0:
        return float("Inf")

    return nts / ntv


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    fasta_file = sys.argv[1]
    with open(fasta_file) as handle:
        seqs = parse_fasta(handle)

    result = trans(seqs[0], seqs[1])

    print(result)
    return


if __name__ == "__main__":
    main()
