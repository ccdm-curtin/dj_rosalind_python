#!/usr/bin/env python3

import sys
import re
import copy

from roz.seq import parse_fasta
from roz.seq import reverse_complement
from roz.seq import seq_translate
from roz.alphabet import dna_codons


usage = """\
To test run:

$ pytest ./distance.py

To run the program on a new dataset run:

$ ./distance.py my_file.txt
"""


def test_orf_finder():

    seq = {
        "id": "Rosalind_99",
        "seq": ("AGCCATGTAGCTAACTCAGGTTACATGGGGATGACCCCGCGACTTGGATTAGAGTCT"
                "CTTTTGGAATAAGCCTGAATGATCCGAGTAGCATCTCAG")
        }

    expected = [
        "MLLGSFRLIPKETLIQVAGSSPCNLS",
        "M",
        "MGMTPRLGLESLLE",
        "MTPRLGLESLLE"
        ]

    actual = orf_finder(seq)
    assert len(actual) == len(expected)

    for orf in expected:
        assert orf in actual

    return


def orf_finder(seq):
    seq_rc = reverse_complement(seq)

    orfs = set()

    #NB Found after solution for forum.
    # This regex finds nested orfs e.g. MGMATA* give 2 orfs.
    # Doesn't return single "M" as in the question though.
    #regex = re.compile("(?=(M.*?)\*)")

    regex = re.compile(r"(M\w*)\*")
    start = re.compile(r"M")
    for se in [seq, seq_rc]:
        for i in [0, 1, 2]:
            s = copy.deepcopy(se)
            s["seq"] = s["seq"][i:]
            tseq = seq_translate(s, table=dna_codons, include_stop=True)
            #print(tseq["seq"])
            matches = regex.findall(tseq["seq"])

            for match in matches:
                for met in start.finditer(match):
                    orfs.add(match[met.start():])
    return orfs


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    file_path = sys.argv[1]
    with open(file_path) as handle:
        seqs = parse_fasta(handle)

    matches = set()

    for seq in seqs:
        matches.update(orf_finder(seq))

    for match in matches:
        print(match)
    return


if __name__ == "__main__":
    main()
