#!/usr/bin/env python3

import sys
import copy

from roz.seq import parse_fasta
from roz.alphabet import translate
from roz.alphabet import rna_codons


usage = """\
To test run:

$ pytest 22-sseq/spliced_motif.py

To run the program on a new dataset run:

$ 22-sseq/spliced_motif.py my_file.fasta
"""

def test_spl():
    example = [
        [
            {
            "id": "Rosalind_14",
            "seq": "ACGTACGTGACG"
            },
            {
            "id": "Rosalind_18",
            "seq": "GTA"
            },
        ],
        [
            {
            "id": "Rosalind_14",
            "seq": "ACGAACGTGACG"
            },
            {
            "id": "Rosalind_18",
            "seq": "GTA"
            },
        ],
    ]

    expected = [[3, 4, 5], [3, 8, 10]]

    for i in range(len(example)):
        actual = spl(example[i][0], example[i][1])

        assert len(actual) == len(expected[i])

        for exp, act in zip(expected[i], actual):
            assert act == exp
    return


def spl(seq1, seq2):
    output = []
    i = 0
    for j, base in enumerate(seq1["seq"]):
        if base == seq2["seq"][i]:
            output.append(j + 1)
            i += 1

            if i >= len(seq2["seq"]):
                return output

    return output


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    fasta_file = sys.argv[1]
    with open(fasta_file) as handle:
        seqs = parse_fasta(handle)

    result = spl(seqs[0], seqs[1])

    print(*result)
    return


if __name__ == "__main__":
    main()
