#!/usr/bin/env python3

import sys
import re
import copy

from itertools import permutations

from roz.alphabet import amino_acid_mass


usage = """\
To test run:

$ pytest ./protein_mass.py

To run the program on a new dataset run:

$ ./protein_mass.py SKADYEK
"""


def test_sum_mass():
    seq = "SKADYEK"
    expected = 821.392
    actual = sum_mass(seq)

    tolerance = 0.0001
    assert ((expected - actual) < tolerance) and ((actual - expected) < tolerance)
    return


def sum_mass(seq):
    return sum([amino_acid_mass[aa] for aa in seq])


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    seq = str(sys.argv[1])
    print(sum_mass(seq))

    return


if __name__ == "__main__":
    main()
