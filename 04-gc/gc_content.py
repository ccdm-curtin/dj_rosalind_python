#!/usr/bin/env python3

import sys

usage = """\
To test run:

$ pytest ./gc_content.py

To run the program on a new dataset run:

$ ./gc_content.py sequences.fasta
"""


def test_fasta_parser():
    example = """\
>Rosalind_6404
CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC
TCCCACTAATAATTCTGAGG
>Rosalind_5959
CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT
ATATCCATTTGTCAGCAGACACGC
>Rosalind_0808
CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC
TGGGAACCTGCGGGCAGTAGGTGGAAT
"""

    expected = {
        "Rosalind_6404": ("CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTT"
                          "CCGGCCTTCCCTCCCACTAATAATTCTGAGG"),
        "Rosalind_5959": ("CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCC"
                          "GCCGAAGGTCTATATCCATTTGTCAGCAGACACGC"),
        "Rosalind_0808": ("CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGA"
                          "CCAGCCCGGACTGGGAACCTGCGGGCAGTAGGTGGAAT")
        }

    seqs = fasta_parser(example.split('\n'))
    for seq_id, seq in expected.items():
        assert seqs[seq_id] == seq
    return


def fasta_parser(handle):
    """ Read fasta formatted file into a dictionary, keyed by seqid.

    Input can be a file-like object, or a list of strings representing each
    line of the file.
    """
    output = dict()

    # Initialise our state storage
    seq_id = None
    seq = ""

    # Loop through each line in the file.
    for line in handle:
        # Stip any extra whitespace on the line.
        sline = line.strip()

        # When the line startswith > we start a new fasta record.
        if sline.startswith('>'):

            # If it's not the first time we've encountered a record in the file
            # add the current seq_id and sequence to the output dict.
            # If seqid is none there will never be a sequence.
            if seq_id is not None:
                output[seq_id] = seq

            # Assign the seqid to the current line.
            # Overwrite the sequence with an empty string.
            seq_id = sline.lstrip("> ")
            seq = ""
        else:
            # If it's not a new record line, join the current line with the
            # sequence string.
            seq += sline

    # Add the final sequence to the output
    # (since it won't encounter a new record).
    output[seq_id] = seq

    return output


def test_gc():
    example = ("CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGA"
               "CCAGCCCGGACTGGGAACCTGCGGGCAGTAGGTGGAAT")

    exp_gc = 60.919540

    gc_content = gc(example)

    # Simple way of evaluating float equality.
    # Could also use math.abs
    assert ((gc_content - exp_gc < 0.001) and (exp_gc - gc_content < 0.001))

    return


def gc(seq):
    """ Given a string, calculates % GC """

    # Seq is a string object.
    # String objects have a method 'count' that does what you'd expect.
    num_g = seq.count('G')
    num_c = seq.count('C')

    # len returns the length of a string or iterable.
    return ((num_g + num_c) / len(seq)) * 100


def test_highest_gc():
    example = [
        ("Rosalind_6404", 53.75),
        ("Rosalind_5959", 53.57142857142857),
        ("Rosalind_0808", 60.919540)
        ]

    # Tuple assignment
    # Possible to use but I don't like it much
    exp_winner, exp_gc = ("Rosalind_0808", 60.919540)

    winner, gc_content = highest_gc(example)

    assert winner == exp_winner

    # Simple way of evaluating float equality.
    # Could also use math.abs
    assert ((gc_content - exp_gc < 0.001) and (exp_gc - gc_content < 0.001))

    return


def highest_gc(gcs):
    """ Find the element with the highest gc content and return it. """

    # Set the maximum gc to some dummy value.
    max_gc = 0
    max_seq = None

    # Loop through each element
    for seq_id, gc_content in gcs:
        # If the current gc is greater than the current max gc, update the max
        # gc value.
        if gc_content > max_gc:
            max_gc = gc_content
            max_seq = seq_id

        # We don't really care about anything else.

    return max_seq, max_gc


def main():
    """ Takes sequence from the command line and prints results. """

    # Check the command line args
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    # Get the path to the fasta file.
    fasta_file = sys.argv[1]

    # Open the file into a file object handle.
    with open(fasta_file, "r") as handle:
        # Read the file handle into a dictionary
        seqs = fasta_parser(handle)

    """
    NB: files need to be closed when you're finished with them.
    'with' is a nice way of automatically closing file handles.
    You could do the same thing as above manually...


    ```
    handle = open(fasta_file, "r")
    seqs = fasta_parser(handle)
    handle.close()
    ```

    But if an error happens between opening and closing the file, the
    file never gets closed, and we may corrupt the file. `with` handles this
    problem for us using exception handlers etc.
    """

    # Calculate the GC% for each sequence
    gcs = [(k, gc(v)) for k, v in seqs.items()]

    # Find which sequence had the highest gc
    winner, gc_content = highest_gc(gcs)

    # Print output in required format
    print(winner)
    print(gc_content)
    return


if __name__ == "__main__":
    main()
