#!/usr/bin/env python3

import sys
import re
import copy


usage = """\
To test run:

$ pytest ./back_translation.py

To run the program on a new dataset run:

$ ./back_translation.py MCGGVA
"""


def mendel(d, h, r):
    n = d + h + r

    p_d = (d / n)

    p_d_h = (h / n) * (d / (n - 1)) * 1
    p_h_h = (h / n) * ((h - 1) / (n - 1)) * 3/4
    p_h_r = (h / n) * (r / (n - 1)) * 1/2 * 2

    p_d_r = (d / n) * (r / (n - 1))

    return sum([p_d, p_d_h, p_h_h, p_h_r, p_d_r])


def main():
    """ Takes sequence from the command line and prints results. """

    if len(sys.argv) < 1 + 3:
        print(usage)
        sys.exit(1)

    n_dom = int(sys.argv[1])
    n_het = int(sys.argv[2])
    n_rec = int(sys.argv[3])

    x = mendel(n_dom, n_het, n_rec)
    print(x)
    return


if __name__ == "__main__":
    main()
