# dj_rosalind_python

Working through the project rosalind problem set using Python.

For simplicity and educational reasons, solutions will not use external libraries and avoid complex data structures where possible.

Each solution will include simple unit tests to demonstrate their utility and how they are used.
Unit tests will use the [pytest](https://docs.pytest.org/en/latest/) package.
