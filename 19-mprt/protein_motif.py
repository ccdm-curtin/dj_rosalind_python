#!/usr/bin/env python3

import sys
import re
import copy

from urllib import request

from roz.seq import parse_fasta

usage = """\
To test run:

$ pytest ./back_translation.py

To run the program on a new dataset run:

$ ./back_translation.py MCGGVA
"""


def fetch_sequence(uniprot_id):
    with request.urlopen(f"http://www.uniprot.org/uniprot/{uniprot_id}.fasta") as response:
        fasta = response.read().decode().split("\n")
    return fasta


def glycosyl_finder(seq):
    # Pattern
    # N !P S|T !P

    s = seq["seq"]

    matches = []

    for i in range(len(s) - 4):
        if s[i] == 'N':
            if s[i + 2] not in ('S', 'T'):
                continue

            if 'P' in (s[i + 1], s[i + 3]):
                continue

            # All checks passed so it's a match
            matches.append(i)
    return matches


def glycosyl_regex(seq):
    import re

    regex = re.compile(r"N[^P][ST][^P]")

def main():
    """ Takes sequence from the command line and prints results. """

    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    with open(sys.argv[1]) as handle:
        uids = [l.strip() for l in handle.readlines()]

    lines = []
    for uid in uids:
        lines.extend(fetch_sequence(uid))

    seqs = parse_fasta(lines)
    for uid, seq in zip(uids, seqs):
        matches = glycosyl_finder(seq)
        if len(matches) > 0:
            #print(seq["id"].split(" ")[0].replace("|", "_"))
            print(uid)
            print(*[i + 1 for i in matches])
    return


if __name__ == "__main__":
    main()
