#!/usr/bin/env python3

import sys
import re
import copy

from roz.seq import parse_fasta
from roz.seq import reverse_complement

usage = """\
To test run:

$ pytest ./13-restriction_sites.py

To run the program on a new dataset run:

$ ./13-restriction_sites.py my_file.fasta
"""


def test_rest_sites():
    example = {
        "id": "Rosalind_24",
        "seq": "TCAATGCATGCGGGTCTATATGCAT"
        }

    expected = [
        (4, 6),
        (5, 4),
        (6, 6),
        (7, 4),
        (17, 4),
        (18, 4),
        (20, 6),
        (21, 4),
        ]

    actual = set(rest_sites(example))

    for exp in expected:
        assert exp in actual

    return


def make_square(n):
    return [[] for _ in range(n)]

def split(s, s_, i):
    i_ = i + 1
    while i >=0 and i_ < len(s):
        yield s[i], s_[i_]
        i -= 1
        i_ += 1

    return


def rest_sites(seq, min_length=4, max_length=12):
    rc = reverse_complement(seq)
    rc = rc["seq"][::-1]
    s = seq["seq"]

    results = list()

    for i in range(len(s)):
        length = 0
        string = []
        for base, base_rc in split(s, rc, i):
            if base != base_rc:
                break

            length += 1
            string.insert(0, base)
            string.append(base_rc)

            if min_length <= length * 2 <= max_length:
                results.append((i - length + 2, length * 2))

    return results


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    file_path = sys.argv[1]
    with open(file_path) as handle:
        seqs = parse_fasta(handle)

    matches = rest_sites(seqs[0])
    for start, length in matches:
        print(start, length)

    return


if __name__ == "__main__":
    main()
