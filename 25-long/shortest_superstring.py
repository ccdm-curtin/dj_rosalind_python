#!/usr/bin/env python3

import sys
import copy
import pprint

from collections import defaultdict

from roz.seq import parse_fasta
from roz.alphabet import translate
from roz.alphabet import rna_codons


usage = """\
To test run:

$ pytest ./trans_tranv.py

To run the program on a new dataset run:

$ ./trans_tranv.py my_file.fasta
"""



def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    k = 6

    index = defaultdict(list)

    fasta_file = sys.argv[1]
    with open(fasta_file) as handle:
        seqs = {s["id"]: s["seq"] for s in parse_fasta(handle)}

    for sid, seq in seqs.items():
        for i in range((len(seq) // 2) + 1):
            kmer = seq[i:i+k]
            index[kmer].append((sid, i))


    matches = {}
    ## sid1: (sid2, sid1_start)

    for sid2, seq2 in seqs.items():
        kmer_matches = index[seq2[:k]]
        these_matches = []

        for sid1, i in kmer_matches:
            if sid1 == sid2:
                continue

            match = True
            seq1 = seqs[sid1]

            for j, (base1, base2) in enumerate(zip(seq1[i:], seq2)):
                #print(base1, base2)
                if base1 != base2:
                    match = False
                    break

            if match:
                these_matches.append((sid1, sid2, i, j))
                #print("", seq1)
                #print(" " * i, seq2)
                #print("-------------------------------")

        min_length = 1000
        best_match = None
        for m in these_matches:
            if m[2] < min_length:
                min_length = m[2]
                best_match = m

        start = None
        if best_match is None:
            start = sid2
        else:
            matches[best_match[0]] = (best_match[1], best_match[2])

    if start is None:
        suffixes = [match[0] for match in matches.values()]
        start = list(set(seqs.keys()).difference(suffixes))
        assert len(start) == 1
        start = start[0]


    sequence = seqs[start]
    node = start
    while True:
        if node not in matches:
            break

        node, j = matches[node]
        next_seq = seqs[node]

        # j slice is to remove overlapping sequence
        sequence += next_seq[-j:]



    print(sequence)
    #print("ATTAGACCTGCCGGAATAC")
    return


if __name__ == "__main__":
    main()
