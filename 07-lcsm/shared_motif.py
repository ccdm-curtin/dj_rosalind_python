#!/usr/bin/env python3

import sys

from roz.seq import parse_fasta

usage = """\
To test run:

$ pytest ./shared_motif.py

To run the program on a new dataset run:

$ ./shared_motif.py my.fasta
"""

def test_lcs():
    seqs = parse_fasta("""\
>Rosalind_1
GATTACA
>Rosalind_2
TAGACCA
>Rosalind_3
ATACA
""".split("\n")
    )

    expected = "AC"
    actual = lcs(seqs)

    assert len(actual) == len(expected)
    return


def lcs(seqs):
    """ Finds the longest common substring between all sequences. """

    # Our initial winner is the empty string.
    winner = ""

    # Loop through indices of sequence 0, up to (not including) last position.
    for i in range(0, len(seqs[0]["seq"]) - 1):

        # Loop through indices of seq 0, starting at 1 to last pos (inclusive).
        for j in range(i + 1, len(seqs[0]["seq"])):
            # Use i and j to slice the string into substrings.
            substring = seqs[0]["seq"][i:j]
            # Search for each substring in the other seqs.
            if all(substring in s for s in [seq["seq"] for seq in seqs[1:]]):
                # All evaluates to true when all elements are true.
                # If the length of the current substring is > current winner,
                # then we have a new winner.
                if len(substring) > len(winner):
                    winner = substring

    return winner


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    # Read the fasta file
    fasta_file = sys.argv[1]
    with open(fasta_file, "r") as handle:
        seqs = parse_fasta(handle)

    print(lcs(seqs))
    return


if __name__ == "__main__":
    main()
