#!/usr/bin/env python3

import sys

from roz.seq import parse_fasta

usage = """\
To test run:

$ pytest ./shared_motif.py

To run the program on a new dataset run:

$ ./shared_motif_sa.py my.fasta
"""

def test_lcs():
    seqs = parse_fasta("""\
>Rosalind_1
GATTACA
>Rosalind_2
TAGACCA
>Rosalind_3
ATACA
""".split("\n")
    )
    expected = "AC"
    print(seqs)
    actual = lcs(seqs)
    print(actual)
    assert len(actual) == len(expected)
    return


def suffixes(seq):
    """ Constructs a naive suffix array """

    output = list()
    seq_id = seq["id"]
    seq_seq = seq["seq"]

    # Loop through each index, take slice i:end for each substring.
    # Note we store a tuple, containing the substring, index, and seqid.
    for i in range(len(seq_seq)):
        output.append( (seq_seq[i:], i, seq_id) )

    # Return the list, sorted by the substrings of each tuple.
    return sorted(output, key=lambda x: x[0])


def lcs_sa(sa, seq_ids):
    """ Searches for longest common prefix in a suffix array, using windows.

    Note that it requires at least 1 representative sequence from each seq
    in seq_ids.
    """

    # Initialise a counter dict to keep track of which seqids are represented.
    seq_tracker = {s: 0 for s in seq_ids}

    # Initialise a list to keep track of the current winner.
    winner = []

    # Initialise the window bounds.
    i = 0
    j = -1

    # Start an infinite loop, we'll have to make sure to terminate!
    while True:
        #print(i, j, seq_tracker)

        # If all seqids have at least one substring in the window, we look
        # for a common prefix.
        if all(v > 0 for v in seq_tracker.values()):
            # Initialise a list to hold the common prefix.
            current = []

            # [c[0] for c in  sa[i:j + 1]] extracts each sequence for window
            # * unpacks that list into arguments for zip
            # zip transforms seqs to we can iterate column wise.
            for col in zip(*[c[0] for c in  sa[i:j + 1]]):
                # If any of the bases in this column is different to the 
                # first base, we break the loop.
                if any(c != col[0] for c in col):
                    break
                # Otherwise we append the base to the common prefix
                else:
                    current.append(col[0])

            # If the common prefix is longer than the current winner, we have
            # a new winner!.
            if len(current) > len(winner):
                winner = current

            # We are going to shift the left bound of the window.
            # So we decrement the count of suffixes for the seqid at that left
            # bound, and increase the value of i.
            # Note that we don't increment the seq_tracker because j already
            # counts it.
            seq_tracker[sa[i][2]] -= 1
            i += 1
            continue
        # Otherwise, we expand the right boundary of the window until each
        # seqid is represented.
        else:
            # Increment right windown bound
            j += 1

            try:
                # Increment the count of the seqid represented in the expanded
                # window.
                seq_tracker[sa[j][2]] += 1
            # But if j >= len(sa) we've reached the end, so an error is raised.
            # We handle that error and use it to break our infinite loop.
            except IndexError:
                break

    # Convert the list to a string and return.
    return "".join(winner)


def lcs(seqs):
    """ Find longest commont substring for seqs.

    NB, could have done this in main, but wanted same interface for testing.
    """

    # Construct suffix arrays for each sequence and merge them
    sa = []
    for seq in seqs:
        sa.extend(suffixes(seq))

    # Sort the overall list.
    sa.sort(key=lambda x: x[0])

    """
    for s in sa:
        print(s)
    """

    # Find the longest common prefix in the suffix array to solve problem.
    winner = lcs_sa(sa, [seq["id"] for seq in seqs])
    return winner


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    # Read the fasta file
    fasta_file = sys.argv[1]
    with open(fasta_file, "r") as handle:
        seqs = parse_fasta(handle)

    print(lcs(seqs))
    return


if __name__ == "__main__":
    main()
