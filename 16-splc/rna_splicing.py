#!/usr/bin/env python3

import sys
import re
import copy

from roz.seq import parse_fasta


usage = """\
To test run:

$ pytest ./rna_splicing.py

To run the program on a new dataset run:

$ ./rna_splicing.py my_file.fasta
"""


from roz.alphabet import translate
from roz.alphabet import dna_codons

def test_splice_finder():
    examples = [
        ("AAAATTTTTTAAATTTTTA", "TTTTTT", 4, 10),
        ("AAAATTTTTGAAATTTTTT", "TTTTTT", 13, 19)
        ]


    for seq, intron, start, stop in examples:
        astart, astop = splice_finder(seq, intron)

        assert astart == start
        assert astop == stop

    return


def splice_finder(seq, intron):
    for i in range(len(seq) - len(intron) + 1):
        full_length = True
        for j in range(len(intron)):
            if seq[i + j] != intron[j]:
                full_length = False
                break

        if full_length:
            return i, i + len(intron)

    return 0, 0


def test_splicer():
    seq = {
        "id": "Rosalind_10",
        "seq": "ATGGTCTACATAGCTGACAAACAGCACGTAGCAATCGGTCGAATCTCGAGAGGCATATGGTCACATGATCGGTCGAGCGTGTTTCAAAGTTTGCGCCTAG"
        }

    introns = [
        {"id": "Rosalind_12", "seq": "ATCGGTCGAA"},
        {"id": "Rosalind_15", "seq": "ATCGGTCGAGCGTGT"},
        ]

    expected = "ATGGTCTACATAGCTGACAAACAGCACGTAGCATCTCGAGAGGCATATGGTCACATGTTCAAAGTTTGCGCCTAG"

    actual = splicer(seq, introns)

    assert expected == actual
    return


def splicer(seq, introns):
    introns = [
        splice_finder(seq["seq"], i["seq"])
        for i
        in introns
        ]

    introns.sort(key=lambda x: x[0])

    exons = []
    start = 0

    for intron in introns:
        exons.append((start, intron[0]))
        start = intron[1]

    if start < len(seq["seq"]):
        exons.append((start, len(seq["seq"])))

    return "".join([seq["seq"][i:j] for i, j in exons])


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    file_path = sys.argv[1]
    with open(file_path) as handle:
        seqs = parse_fasta(handle)

    cds = splicer(seqs[0], seqs[1:])
    aa = translate(cds, table=dna_codons, include_stop=False)
    print(aa)
    return


if __name__ == "__main__":
    main()
