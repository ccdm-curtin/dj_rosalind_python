#!/usr/bin/env python3

import sys
import copy

from roz.seq import parse_fasta
from roz.alphabet import translate
from roz.alphabet import rna_codons


usage = """\
To test run:

$ pytest ./distance.py

To run the program on a new dataset run:

$ ./distance.py my_file.txt
"""


def test_fib():
    n = 5
    k = 3

    expected = 19
    actual = fib(n, k)

    assert actual == expected
    return


def recurrence(n, m, memo, i=0):
    if i >= n:
        return 0
    elif i + m >= n:
        n_gen = n - i
        count = 1
    else:
        n_gen = m
        count = 0

    for j in range(1, n_gen):
        k = i + j + 1
        if k in memo:
            result = memo[k]
        else:
            result = recurrence(n, m, memo, i=k)
            memo[k] = result

        count += result
    return count


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 3:
        print(usage)
        sys.exit(1)

    n = int(sys.argv[1])
    m = int(sys.argv[2])

    print(recurrence(n, m, dict()))
    return


if __name__ == "__main__":
    main()
