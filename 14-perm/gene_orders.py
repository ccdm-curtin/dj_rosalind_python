#!/usr/bin/env python3

import sys
import re
import copy

from itertools import permutations

usage = """\
To test run:

$ pytest ./gene_orders.py

To run the program on a new dataset run:

$ ./gene_orders.py 3
"""


def test_factorial():
    examples = [3, 4, 8, 30]
    expecteds = [6, 24, 40320, 265252859812191058636308480000000]

    for example, expected in zip(examples, expecteds):
        assert factorial(example) == expected
    return


def factorial(n):
    """ Finds the factorial of a number n.

    NB will fail for n >= 1000 because of recursion limits in python.
    """
    if n < 0:
        raise ValueError("Factorial of negative numbers undefined.")
        return
    # By definition 1! = 1 and 0! = 1
    elif n <= 1:
        return 1
    # n! = n * (n - 1) * (n - 2) * ... * 1
    else:
        # Find solution by recursion
        return n * factorial(n - 1)


def test_permute():
    examples = [
        [
            [0, 1],
            2,
            [(0, 1), (1, 0)]
        ],
        [
            [0, 1, 2],
            6,
            [(0, 1, 2), (0, 2, 1), (1, 0, 2),
             (1, 2, 0), (2, 0, 1), (2, 1, 0)]
        ],
        ]

    for example in examples:
        ex_arr, n, expected = example
        actual = permute(ex_arr, n)
        assert len(expected) == len(actual)

        actual = set(actual)
        for perm in expected:
            assert perm in actual

    return


def permute(arr, n):
    """ Permutes an input array n times.
    If n is len(arr) factorial then all possible permutations will be returned.
    """
    output = [tuple(arr)]

    for i in range(n - 1):
        j = i % (len(arr) - 1)
        tmp = arr[j]
        arr[j] = arr[j + 1]
        arr[j + 1] = tmp
        output.append(tuple(arr))

    return output


def main():
    """ Takes sequence from the command line and prints results. """
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    n = int(sys.argv[1])

    n_fact = factorial(n)
    print(n_fact)

    perms = permutations(list(range(1, n + 1)))
    for perm in perms:
        print(*perm)
    return


if __name__ == "__main__":
    main()
